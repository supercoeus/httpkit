/**
 * @author mdc
 * @date 2017年7月5日 下午5:20:03
 */
package com0oky.httpkit.test;

import java.io.File;
import java.util.Map;

import org.junit.Test;

import com0oky.httpkit.http.HttpKit;
import com0oky.httpkit.http.ResponseWrap;
import com0oky.httpkit.http.request.RequestBase;

/**
 * @author mdc
 * @date 2017年7月5日 下午5:20:03
 */
public class SimpleTest {
	
	@Test
	public void proxyTest(){
		String url ="https://my.oschina.net";
		String html = HttpKit.get(url).setProxy("192.168.10.10", 1182).execute().getString();
		System.out.println(html);
	}
	
	//保持Session
	@Test
	public void withSessionTest(){
		String url = "https://www.oschina.net/action/user/hash_login";
		String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
		
		RequestBase request = HttpKit.post(url)
		.addParameter("email", "xxxxxxx")
		.addParameter("pwd", Sha1Util.getSha1("xxxxxxx"))
		.addParameter("verifyCode", "")
		.addParameter("save_login", 1)
		
		.addHeader("Origin", "https://www.oschina.net")
		.setUserAgent(userAgent);
		
		ResponseWrap response = request.execute();
		
		String resultString = response.getString();
		if (resultString != null && resultString.trim().length() > 0) {
			Map<?, ?> json = response.getJson(Map.class);
			System.out.println("HttpKit登录失败===>" + json.get("msg"));
			return ;
		}
		
		System.out.println(response.getString());
		System.out.println("登录成功===>" + response.getStatusLine());
		
		//跳转到一个页面, 保持Session, 只需要把登录的HttpKit传到下一次请求即可
		String html = HttpKit.get("https://my.oschina.net/yiq", request)
		.setUserAgent(userAgent)
		.execute().getString();
		
		String userStr = "owner_id\" data-value=\"";
		int dataUserIndex = html.indexOf(userStr) + userStr.length();
		
		//获取到UserId
		String userId = html.substring(dataUserIndex, html.indexOf("\">", dataUserIndex));
		
		System.out.println("获取到UserId===>" + userId);
	}
	
	@Test
	public void sslTest(){
		String string = HttpKit.post("https://wwwt.est.com")
		.setJKS(new File("jksFile.jks"), "xxxx")
		.execute().getString();
		
		System.out.println(string);
	}
}
